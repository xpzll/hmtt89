import Vue from 'vue'
import Vuex from 'vuex'
import { loginAPI, userInfoAPI } from '@/api/user'
import { Toast } from 'vant';
// 导入操作token的工具
import { getToken, setToken, removeToken } from '@/utils/auth'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 保存token的一个对象
    tokenObj: getToken() || {},
    // 专门用来保存用户信息的资料
    userInfo: {}
  },
  // 放同步方法的地方，也是唯一修改state的地方
  mutations: {
    // 修改token
    changeToken(state, obj) {
      // 把传递过来的token对象存到state
      state.tokenObj = obj
      // 也存到本地存储
      setToken(obj)
    },

    // 做退出
    logout(state) {
      // 把vuex里的赋值为空
      state.tokenObj = {}
      // 删除本地存储里的token
      removeToken()
    },

    // 专门用来修改userInfo的方法
    setUserInfo(state, obj) {
      state.userInfo = obj
    }
  },
  // 放异步方法的地方
  actions: {

    async login(ctx, info) {
      try {
        const res = await loginAPI(info)
        // 把得到的vuex提交给mutations
        // mutations内部给了state
        ctx.commit('changeToken', res.data)
        Toast.success('登录成功')
        return true // 告诉外界登录成功

      } catch {

        Toast.fail('验证码错误！')
        return false // 告诉外界调用者登录失败
      }
    },

    // 如果一个vuex里的数据是异步请求来的，就要在actions提供一个方法来获取
    async reqUserInfo(ctx) {
      // 当前vuex里如果没有数据才发请求
      // 有数据就不发请求了
      if (!ctx.state.userInfo.name) {
        // 发请求去获取资料
        const res = await userInfoAPI()

        // 通过提交的方式来间接存到state
        ctx.commit('setUserInfo', res.data)
      }
    }
  },

})
