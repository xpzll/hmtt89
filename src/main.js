import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 导入基本全局样式
import './styles/base.less'
// 导入vant
import './utils/vant'
// 导入flexible
import 'amfe-flexible'

// 这个导入的语法叫什么规范？
// es6的语法： import 变量名 from '路径'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
dayjs.extend(relativeTime)

// 需要用到dayjs的国际化
import 'dayjs/locale/zh-cn'
dayjs.locale('zh-cn')

// 声明一个过滤器，专门用来处理相对时间
Vue.filter('relvTime', (val) => {

  return dayjs(val).fromNow()
})


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')