// 这个模块主要封装跟评论有关的接口
import request from '@/utils/request'

// 封装一个获取评论或回复的接口
export const commentAPI = (params) => {

    return request({
        url: '/v1_0/comments',
        params
    })
}

// 封装一个获取评论或回复的接口
export const sendCmtAPI = (data) => {

    return request({
        url: '/v1_0/comments',
        method: 'post',
        data
    })
}