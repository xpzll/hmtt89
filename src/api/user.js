// 导入请求对象
import request from '@/utils/request'
// 导入store
import store from '@/store'

// 封装一个用来做登录的接口函数
export const loginAPI = (data) => {

    return request({
        // 不能有空格！不能有空格！不能有空格
        url: '/v1_0/authorizations',
        // 这里不是vue里面，这里就是axios
        // axios的请求方式就叫method，不要写成methods
        method: 'post',
        data
    })
}


// 封装一个获取登录用户个人资料的接口
export const userInfoAPI = () => {
    
    return request({
        url: '/v1_0/user/profile',
    })
}

// 封装一个修改用户资料的接口
export const editInfoAPI = (data) => {

    return request({
        url: '/v1_0/user/profile',
        method: 'patch',
        data
    })
}

// 封装一个修改用户头像的接口
export const editPhotoAPI = data => {

    return request({
        url: '/v1_0/user/photo',
        method: 'patch',
        data
    })
}