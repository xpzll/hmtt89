// 本文件以后专门封装跟新闻有关的接口

import request from '@/utils/request'

// 封装一个获取新闻推荐的接口
export const articleListAPI = (params) => {

    return request({
        url: '/v1_0/articles',
        method: 'get',
        params
    })
}

// 封装一个获取新闻详情的接口
export const detailAPI = (id) => {

    return request({
        url: `/v1_0/articles/${id}`
    })
}