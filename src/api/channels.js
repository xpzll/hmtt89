// channels：频道的意思
// 所以以后这个文件专门用来封装跟频道有关的接口
import request from '@/utils/request'

// 封装一个获取用户自己频道的接口
export const myChannelsAPI = () => {

    return request({
        url: '/v1_0/user/channels'
    })
}


// 封装一个获取所有频道的接口
export const allChannelsAPI = () => {

    return request({
        url: '/v1_0/channels'
    })
}


// 封装一个用来重置我的频道的接口（既可以新增也可以删除）
export const setChannelsAPI = (data) => {

    return request({
        url: '/v1_0/user/channels',
        method: 'put',
        data
    })
}