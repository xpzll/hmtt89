import Vue from 'vue'
import VueRouter from 'vue-router'
// 导入Store
import store from '@/store'
// 导入toast
import { Toast } from 'vant'
// 导入组件 -- 普通导入方式-没有懒加载
// import MyLogin from '@/views/MyLogin'
// import Layout from '@/views/Layout'
// import MyHome from '@/views/MyHome'
// import MyAsk from '@/views/MyAsk'
// import MyVideo from '@/views/MyVideo'
// import MyMine from '@/views/MyMine'
// import MineEdit from '@/views/MyMine/MineEdit'
// import MyArticle from '@/views/MyArticle'
// import MySearch from '@/views/MySearch'
// import result from '@/views/MySearch/result'
// import chat from '@/views/MyMine/chat'

// 路由懒加载形式
const MyLogin = () => import('@/views/MyLogin')
const Layout = () => import('@/views/Layout')
const MyHome = () => import('@/views/MyHome')
const MyAsk = () => import('@/views/MyAsk')
const MyVideo = () => import('@/views/MyVideo')
const MyMine = () => import('@/views/MyMine')
const MineEdit = () => import('@/views/MyMine/MineEdit')
const MyArticle = () => import('@/views/MyArticle')
const MySearch = () => import('@/views/MySearch')
const result = () => import('@/views/MySearch/result')
const chat = () => import('@/views/MyMine/chat')

Vue.use(VueRouter)

const routes = [
  // 路由重定向
  { path: '/', redirect: '/layout/home' },
  { name: 'login', path: '/login', component: MyLogin },
  { name: 'MyArticle', path: '/article', component: MyArticle },
  { name: 'MySearch', path: '/search', component: MySearch },
  { name: 'result', path: '/search/result', component: result },
  { name: 'chat', path: '/mine/chat', component: chat },
  {
    name: 'edit',
    path: '/mine/edit',
    component: MineEdit,
    meta: {
      needLogin: true
    }
  },
  {
    name: 'layout',
    path: '/layout',
    component: Layout,
    children: [
      {
        name: 'home',
        path: 'home',
        component: MyHome,
        meta: {
          needKeep: true
        }
      },
      { name: 'ask', path: 'ask', component: MyAsk },
      { name: 'video', path: 'video', component: MyVideo },
      {
        name: 'mine',
        path: 'mine',
        component: MyMine,
        meta: {
          needLogin: true
        }
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

// 解决3.1版本后在控制台出现的警告
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}


// to: 去哪 有要去的路由的信息
// from： 从哪来 有从哪来的路由的信息
// next: 是一个函数，调用这个函数就代表放行，不调用就代表不放行
//        next可以像 $router.push一样使用  next('路径') 代表放行到指定路径
router.beforeEach((to, from, next) => {
  // 判断要去的路由是否有needLogin的标记
  if (to.meta.needLogin) {
    // 如果有代表去的是要登录的页面
    // 判断有没有token
    if (store.state.tokenObj.token) {
      // 代表登录了，直接放行
      next()
    } else {
      // 没登录，打回登录页
      Toast('请先登录')
      // 打回登录页携带参数
      next({
        name: 'login',
        query: {
          // 参数名叫back
          // 参数值就是我原本想去的路径
          back: to.path
        }
      })
    }
  } else {
    // 代表去的是不需要登录的页面，直接放行
    next()
  }
})

export default router
