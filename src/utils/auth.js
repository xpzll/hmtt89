// auth：权限的意思
// token就是用来鉴权的
// 所以在这个文件里专门封装本地存储保存token的东西

// 声明成常量
const TOKEN_KEY = 'hmtt89'

// 保存
export const setToken = (obj) => {

    localStorage.setItem(TOKEN_KEY, JSON.stringify(obj))
}

// 获取 - 为了拿，就意味着要返回
export const getToken = () => {

    return JSON.parse(localStorage.getItem(TOKEN_KEY))
}


// 删除 - 没有参数没有返回值
export const removeToken = () => {

    localStorage.removeItem(TOKEN_KEY)
}