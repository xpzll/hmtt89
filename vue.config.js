const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 打包后生成的文件导入形式用相对路径来导入
  publicPath: './',
  // 打包正式上线时不要生成map文件
  productionSourceMap: false
})
